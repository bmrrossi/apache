window.camTasklistConf = {
// change the app name and vendor
// app: {
//   name: 'Todos',
//   vendor: 'Company'
// },
//
// configure the date format
// "dateFormat": {
//   "normal": "LLL",
//   "long":   "LLLL"
// },
//
  'locales': {
    'availableLocales': ['en', 'de', 'pt'],
    'fallbackLocale': 'pt'
  },
//
// // custom libraries and scripts loading and initialization,
// // see: http://docs.camunda.org/guides/user-guide/#tasklist-customizing-custom-scripts
// customScripts: {
//   // AngularJS module names
//   ngDeps: ['ui.bootstrap'],
//   // RequireJS configuration for a complete configuration documentation see:
//   // http://requirejs.org/docs/api.html#config
//   deps: ['jquery', 'custom-ui'],
//   paths: {
//     // if you have a folder called `custom-ui` (in the `scripts` folder)
//     // with a file called `scripts.js` in it and defining the `custom-ui` AMD module
//     'custom-ui': 'custom-ui/scripts'
//   }
// },
  'shortcuts': {
    'claimTask': {
      'key': 'ctrl+alt+c',
      'description': 'Chamar a tarefa atual selecionadas'
    },
    'focusFilter': {
      'key': 'ctrl+shift+f',
      'description': 'Focar no primeiro filtro da lista'
    },
    'focusList': {
      'key': 'ctrl+alt+l',
      'description': 'Focar na primeira tarefa da lista'
    },
    'focusForm': {
      'key': 'ctrl+alt+f',
      'description': 'Focar no primeiro campo da tarefa'
    },
    'startProcess': {
      'key': 'ctrl+alt+p',
      'description': 'Abrir o modal de Iniciar Processo'
    }
  }
};